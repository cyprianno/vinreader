/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package pl.zua.vinreader;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.zua.vinreader.fxui.controllers.MainAppController;

/**
 * @author Cyprian �niegota
 * @since 1.0
 */
public class VinreaderFxApp extends Application {

	public VinreaderFxApp() {
	}

	public static void main(String[] args) {
		VinreaderFxApp.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader f = new FXMLLoader();
		final Parent fxmlRoot = (Parent) f.load(VinreaderFxApp.class.getResourceAsStream("/fxui/main.fxml"));
		MainAppController controller = f.getController();
		controller.setStage(stage);
		stage.setScene(new Scene(fxmlRoot));
		stage.show();
	}
}
