/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package pl.zua.vinreader.fxui.controllers;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.zua.vinreader.domain.model.UfgConnection;
import pl.zua.vinreader.domain.model.UfgRequest;
import pl.zua.vinreader.domain.model.UfgResponse;
import pl.zua.vinreader.domain.model.enums.ErrorCode;
import pl.zua.vinreader.domain.services.UfgVinService;
import pl.zua.vinreader.domain.services.VinFileReader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Cyprian �niegota
 * @since 1.0
 */
public class MainAppController implements Initializable {
	@FXML
	private MenuBar menuBar;

	@FXML
	Label toolbarLabel;

	@FXML
	ProgressBar progressBar;

	@FXML
	ImageView imageView;

	@FXML
	TextField textField;

	private UfgVinService ufgVinService = new UfgVinService();

	private Stage stage;
	private List<String> vins = new ArrayList<>();
	private ConcurrentLinkedQueue<String> vinsQueue = new ConcurrentLinkedQueue<>();
	private ConcurrentLinkedQueue<String> vinsProgress = new ConcurrentLinkedQueue<>();

	long startTime;

	private List<UfgResponse> responseList = Collections.synchronizedList(new ArrayList<UfgResponse>());

	private UfgConnection currentConnection;
	ExecutorService executorService = Executors.newFixedThreadPool(10);

	@FXML
	private void handleAboutAction(final ActionEvent event) {
		provideAboutFunctionality();
	}

	@FXML
	private void handleSaveFileAction(final ActionEvent event) {
		provideSaveFileFunctionality();
	}

	@FXML
	private void handleExitAction(final ActionEvent event) {
		provideExitFunctionality();
	}

	@FXML
	private void handleOpenFileAction(final ActionEvent event) {
		provideOpenFileFunctionality();
	}

	private void provideExitFunctionality() {
		System.exit(0);
	}

	private void provideSaveFileFunctionality() {
		if (this.responseList.isEmpty()) {
			this.toolbarLabel.setText("No data to save");
			return;
		}
		for (UfgResponse ufgResponse : this.responseList) {
			if (ufgResponse.getErrorCode() != null) {
				System.out.println(
						ufgResponse.getVin() + ";" + ufgResponse.getContractNumber() + ";" + ufgResponse.getCarName() + ";" + ufgResponse.getCompanyName() + ";"
								+ ufgResponse.getCompanyAddress());
			} else {
				System.out.println(ufgResponse.getVin() + ";" + ufgResponse.getErrorCode());
			}
		}
		this.toolbarLabel.setText("Zapisywanie zako�czone.");

	}

	private void readNextCaptcha() {
		executorService.submit(new Runnable() {
			@Override
			public void run() {
				synchronized (MainAppController.this) {
					try {
						currentConnection = ufgVinService.getConnection();
						byte[] bytes = currentConnection.getCaptchaBytes();
						BufferedImage convert = convert(bytes);
						WritableImage writableImage = SwingFXUtils.toFXImage(convert, null);
						imageView.setImage(writableImage);
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
				}
			}
		});
	}

	public BufferedImage convert(byte[] b) throws IOException {
		ByteArrayInputStream bais = new ByteArrayInputStream(b);
		return ImageIO.read(bais);
	}

	private void provideOpenFileFunctionality() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		File file = fileChooser.showOpenDialog(stage);
		if (file != null) {
			try {
				this.vins.clear();
				this.vinsQueue.clear();
				this.responseList.clear();
				this.vins.addAll(new VinFileReader().readFile(file));
				this.vinsQueue.addAll(this.vins);
				progressBar.setProgress(0f);
				if (this.vins.isEmpty()) {
					toolbarLabel.setText("Brak poprawnych danych w pliku");
					this.imageView.setImage(null);
				} else {
					toolbarLabel.setText("Wczytane [" + this.vins.size() + "] numer�w vin");
					readNextCaptcha();
					startTime = new Date().getTime();
				}
			} catch (IOException e) {
				toolbarLabel.setText("B��d wczytywania pliku [" + file.toString() + "], " + e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
	}

	private void provideAboutFunctionality() {
		//TODO: about dialog
	}

	private void executeAsyncFetch(final String captcha, final UfgConnection connection) {
		executorService.submit(new FetchDataTask(captcha, connection));
	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		menuBar.setFocusTraversable(true);
		textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event1) {
				if (KeyCode.ENTER.equals(event1.getCode())) {
					String captcha = textField.getText();
					toolbarLabel.setText("");
					if (vinsQueue.isEmpty()) {
						toolbarLabel.setText(String.format("All done"));
						return;
					}

					executeAsyncFetch(captcha, currentConnection);

					textField.setText("");
					if (vinsQueue.isEmpty()) {
						long diff = new Date().getTime() - startTime;
						toolbarLabel.setText(String.format("All done in [%d] seconds", (diff / 1000)));
						imageView.setImage(null);
						return;
					}
					readNextCaptcha();
				}
			}
		});
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		executorService.shutdown();
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	private class FetchDataTask extends Task<String> {

		private String captcha;
		UfgConnection connection;

		public FetchDataTask(String captcha, UfgConnection connection) {
			this.captcha = captcha;
			this.connection = connection;
		}

		@Override
		protected String call() throws Exception {
			try {
				String currentVin = vinsQueue.poll();
				vinsProgress.add(currentVin);
				UfgRequest ufgRequest = new UfgRequest();
				ufgRequest.setVin(currentVin);
				ufgRequest.setCaptcha(captcha);
				ufgRequest.setCookies(connection.getCookies());
				UfgResponse ufgResponse = ufgVinService.fetchData(connection, ufgRequest);
				vinsProgress.remove(ufgResponse.getVin());
				if (ErrorCode.CAPTCHA_ERR.equals(ufgResponse.getErrorCode())) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							toolbarLabel.setText("Incorrect captcha");
						}
					});

					vinsQueue.add(ufgResponse.getVin());
					if (imageView.getImage() == null) {
						readNextCaptcha();
					}
					System.out.println("incorrect captcha");
				} else {
					responseList.add(ufgResponse);
					double responseListSize = responseList.size();
					double allListSize = vins.size();
					double progress = responseListSize / allListSize;
					System.out.println("progress:" + progress + ";vins:" + allListSize + ";vinsTodo:" + vinsQueue.size() + ";done:" + (vins.size()
							- vinsQueue.size()) + ";resp:" + responseListSize);
					progressBar.setProgress(progress);
				}
				if (responseList.size() == vins.size()) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							long diff = new Date().getTime() - startTime;
							toolbarLabel.setText(String.format("All done in [%d] seconds", (diff / 1000)));
						}
					});
					imageView.setImage(null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "ok";
		}
	}
}
