/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package pl.zua.vinreader.domain.services;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pl.zua.vinreader.domain.model.UfgConnection;
import pl.zua.vinreader.domain.model.UfgRequest;
import pl.zua.vinreader.domain.model.UfgResponse;
import pl.zua.vinreader.domain.model.enums.ErrorCode;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @author Cyprian �niegota
 * @since 1.0
 */
public class UfgVinService {

	public static final String UFG_MAINPAGE_URL = "https://zapytania.oi.ufg.pl/SASStoredProcess/guest";

	public UfgConnection getConnection() throws IOException {
		UfgConnection ufgConnection = new UfgConnection();
		Connection connection = Jsoup.connect(UFG_MAINPAGE_URL);
		Connection.Response response = connection.execute();
		ufgConnection.getCookies().putAll(response.cookies());
		Document mainPage = response.parse();
		connection = Jsoup.connect("https://zapytania.oi.ufg.pl/SASStoredProcess/guest?form=zpp1&lang=pl");
		for (Map.Entry<String, String> cookie : ufgConnection.getCookies().entrySet()) {
			connection.cookie(cookie.getKey(), cookie.getValue());
		}
		response = connection.execute();
		ufgConnection.getCookies().putAll(response.cookies());
		Document formPage = response.parse();
		Date date = new Date();
		Connection.Response resultImageResponse = Jsoup.connect("https://zapytania.oi.ufg.pl/SASStoredProcess/captcha?ct="+date.getTime()).cookies(ufgConnection.getCookies()).ignoreContentType(true).execute();
		ufgConnection.setCaptchaBytes(resultImageResponse.bodyAsBytes());
		return ufgConnection;
	}

	public UfgResponse fetchData(UfgConnection ufgConnection, UfgRequest ufgRequest) {
		UfgResponse ufgResponse = new UfgResponse();
		ufgResponse.setVin(ufgRequest.getVin());
		try {
			Connection connection = Jsoup.connect(
					"https://zapytania.oi.ufg.pl/SASStoredProcess/register?=&_p_Jezyk=pl&_p_Kraj=99&_p_NaDzien=2015-04-17&_p_NrRejestracyjny=&_p_NrVIN="
							+ ufgRequest.getVin() + "&_p_RodzajZapytania=ZPP1&captcha_response=" + ufgRequest.getCaptcha());
			Connection.Response response = connection.cookies(ufgConnection.getCookies()).method(Connection.Method.POST).timeout(60000).execute();
			ufgConnection.getCookies().putAll(response.cookies());
			ufgResponse.setFullHtml(response.body());
			Document responsePage = response.parse();
			Elements select = responsePage.select("ol li b");
			if (select == null || select.isEmpty()) {
				ufgResponse.setErrorCode(ErrorCode.DATA_ERR);
				return ufgResponse;
			}
			ufgResponse.setContractNumber(getElementHtml(select, 0));
			ufgResponse.setCarName(getElementHtml(select, 1));
			ufgResponse.setCompanyName(getElementHtml(select, 2));
			ufgResponse.setCompanyAddress(getElementHtml(select, 3));
		} catch (UnsupportedMimeTypeException mimeException) {
			ufgResponse.setErrorCode(ErrorCode.CAPTCHA_ERR);
		} catch (IOException ioExeption) {
			ufgResponse.setErrorCode(ErrorCode.IO_ERR);
		} catch (Exception e) {
			ufgResponse.setErrorCode(ErrorCode.UNKNOWN_EXCEPTION);
		}
		return ufgResponse;
	}

	private String getElementHtml(Elements elements, int index) {
		if (elements.size() < index) {
			return "b/d";
		}
		return elements.get(index).html();
	}
}
